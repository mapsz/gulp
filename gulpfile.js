var gulp = require('gulp'),
    livereload = require('gulp-livereload');

//HTML
gulp.task('html',function(){
  gulp.src('./src/**/*.html')
  .pipe(livereload());
})

gulp.task('watch', function(){
  livereload.listen();
  gulp.watch('./src/**/*.html',['html']);
})

